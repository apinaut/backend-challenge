package com.apiomat.csm.application.notifications;

import com.apiomat.csm.application.booking.Booking;

public class MailSender
{
	public void send( final Booking booking )
	{
		System.out.println( "sending email for: " + booking );
	}
}
