package com.apiomat.csm.application.entities.blocks;
public class BookingBlock
{
	private int sortIndex;

	public int getSortIndex( )
	{
		return this.sortIndex;
	}

	public void setSortIndex( final int sortIndex )
	{
		this.sortIndex = sortIndex;
	}
}
