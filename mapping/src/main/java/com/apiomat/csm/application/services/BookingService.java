package com.apiomat.csm.application.services;

import com.apiomat.csm.application.dtos.BookingDto;
import com.apiomat.csm.application.entities.Booking;
import com.apiomat.csm.application.mappers.BookingMapper;
import com.apiomat.csm.application.repositories.BookingRepository;

import java.util.ArrayList;
import java.util.List;

public class BookingService
{
	private final BookingRepository repository;
	private final BookingMapper mapper;

	public BookingService( final BookingRepository repository, final BookingMapper mapper )
	{
		this.repository = repository;
		this.mapper = mapper;
	}

	public List<BookingDto> list( )
	{
		final List<Booking> bookings = this.repository.all( );
		// TODO
		return new ArrayList<>( );
	}

	public BookingDto findById( final Long id )
	{
		final Booking booking = this.repository.findBy( id );
		// TODO
		return new BookingDto( );
	}

	public BookingDto create( final BookingDto bookingDto )
	{
		final Booking create = this.mapper.map( bookingDto );
		this.repository.create( create );
		return this.mapper.map( create );
	}
}
