package com.apiomat.csm.application.dtos.blocks;

public class BookingBlockDto
{
	private int sortIndex;

	public int getSortIndex( )
	{
		return this.sortIndex;
	}

	public void setSortIndex( final int sortIndex )
	{
		this.sortIndex = sortIndex;
	}
}
